

"""
IMPORTANT!!

NEVER run this script directly!
This script is intended to be run remotely.

It will print a dictionary back with the HASH values or the string 'txt' of 
the files which are supposed to be compared. Files supposed to be compared are 
specified in the JSON_CONTENT dictionary copied dynamically to this script 
just before excuting it.

JSON_CONTENT dictionary has this form:
{
	"directories": [],
	"exclude_files": [],
    "include_files": []
}

Dictionary to be printed as a result of running this script has this form:
{ <complete_file_name>: <hash_code | "txt"> }

If there is an error, this dictionary will be printed back:
{ "error": <error_message> }
"""

import json
from os import path

import subprocess

FIND_COMMAND = "find \"{directory}\" -type f {exclude_list} {include_list}"

def _get_all_files(conf_data):
    """Retrieve files according to the rules defined in conf_data. Return a 
    set of those files.

    Search of the files is done by creating a 'find' command in bash. The 
    rules for excluding and including files allow to make searches like this:

        find "/home/intergate" -type f ! -name '*TST.env*' \( -name '*.env' -o -name '*9.jar*' \)

    By returning a set, it's guaranteed no file will be duplicated.
    """
    all_directories = [directory for directory in conf_data['directories']]
    files = set([])
    excludes = ""
    for exc in conf_data['exclude_files']:
        excludes += (" ! -name '{0}'".format(exc))
    includes = ""
    if conf_data['include_files']:
        includes = " -o ".join("-name '{0}'".format(inc) \
                    for inc in conf_data['include_files'])
        includes = "\\( " + includes + " \\)"
    for directory in set(all_directories):
        find_command = FIND_COMMAND.format(directory=directory, 
                                           exclude_list=excludes, 
                                           include_list=includes)
        p = subprocess.Popen(find_command, stdin=subprocess.PIPE, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, dummy = p.communicate()
        for _file in stdout.strip().split('\n'):
            if _file:
                files.add(_file)
    return files

def _is_binary(_file):
    """Determine whether _file is binary or not.
    Please note this function might have some false positives and false 
    negatives, so use it as a helper to another checker alternative.
    """
    textchars = bytearray(
        set([7,8,9,10,12,13,27]) | set(range(0x20, 0x100)) - set([0x7f]))
    is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
    isb = False
    with open(_file, 'rb') as _f:
        isb = is_binary_string(_f.read(1024))
    return isb

def _file_hash(_file):
    """Get and return the hash of _file using md5 algorithm."""
    import hashlib
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(_file, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()

def analize_files(conf_data):
    """Create a dictionary with the keys being the name of the files to be 
    compared, and the values the hash code if the file is binary, or the 
    text 'txt' if the file is plain text."""
    result = {}
    try:
        files = _get_all_files(conf_data)
        for _file in files:
            if _is_binary(_file):
                result[_file] = _file_hash(_file)
            else:
                result[_file] = 'txt'
    except KeyError as ke:
        result['error'] = ("Mandatory key was missing in the data sent to " + 
                          "the remote script: {0}").format(str(ke))
    except Exception as e:
        result['error'] = str(e)
    return json.dumps(result)

# parameter JSON_CONTENT should be added through source code to this file 
print(analize_files(JSON_CONTENT))
