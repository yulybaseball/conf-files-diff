"""
Module that prepares the report and email body to be sent when comparing 
files between servers.
"""

from datetime import date
from os import path, system
import zipfile

EMAIL_SUBJECT = "Files Comparer - {conf_file}"
SEND_EMAIL_COMMAND = " | mailx -s \"{subject}\" {email_addresses}"
SEND_EMAIL_NO_ATTACHMENT_COMMAND = "echo \"{email_body}\" {send_email_command}"
SEND_EMAIL_ATTACHMENT_COMMAND = "(echo \"{email_body}\" ; uuencode {zip_path} {zip_name}) {send_email_command}"
REPORT_FILE_NAME = "report-{conf_file}-{date}.txt"
REPORT_NAME = "report-{conf_file}-{date}"
ZIP_FILE_NAME = "{report_name}.zip"
REPORT_FILE_NAME = "{report_name}.txt"

def _replace_file_by_server_name(servers, file_line):
    """Replace the words File1 and File2 by the name of the servers used 
    in the comparison.
    servers has this form: main_server != other_server
    """
    servers_list = servers.split(' != ')
    main_server = servers_list[0]
    other_server = servers_list[1]
    if file_line.find('\nFile1:') != -1:
        rep_line = "\n{0}:".format(main_server)
        file_line = file_line.replace('\nFile1:', rep_line)
    if file_line.find('\nFile2:') != -1:
        rep_line = "\n{0}:".format(other_server)
        file_line = file_line.replace('\nFile2:', rep_line)
    return file_line

def _create_report_body(differences):
    """Create and return the body of the report by concatenating the 
    differences.
    """
    report_body = ""
    for server_name in differences:
        for diff_key, diffs_msgs in differences[server_name].iteritems():
            if diffs_msgs:
                count_chars = len(diff_key) + 1
                report_body += ("=" * count_chars + "\n")
                report_body += (diff_key + "\n")
                report_body += ("=" * count_chars + "\n")
                for diff_msg in diffs_msgs:
                    line = _replace_file_by_server_name(diff_key, diff_msg)
                    report_body += (line + '\n')
            if report_body:
                report_body += '\n'
    return report_body

def _create_zip_file(report_text_file, report_name, root_dir):
    """Zip the report file and return its complete path."""
    if not report_text_file:
        return None
    zip_file_name = ZIP_FILE_NAME.format(report_name=report_name)
    zip_complete_path = path.join(root_dir, zip_file_name)
    zip_file = zipfile.ZipFile(zip_complete_path, 'a')
    try:
        zip_file.write(report_text_file, path.basename(report_text_file), 
                       compress_type=zipfile.ZIP_DEFLATED)
    except:
        return None
    finally:
        zip_file.close()
    return zip_complete_path if path.isfile(zip_complete_path) else None

def _get_current_date():
    """Get and return current date on this format:
        Month-Day-Year
    where Month is the represented by the first 3 letters of its name.
    e.g. Sep-16-2019
    """
    return date.today().strftime("%b-%d-%Y")

def _create_report_file(differences, conf_file, root_dir):
    """Create the plain report file, zip it and return the zipped one."""
    report_body = _create_report_body(differences)
    if not report_body:
        return None
    current_date = _get_current_date()
    report_name = REPORT_NAME.format(conf_file=conf_file, date=current_date)
    report_file = REPORT_FILE_NAME.format(report_name=report_name)
    plain_report_file_path = path.join(root_dir, report_file)
    with open(plain_report_file_path, "w+") as prf:
        prf.write(report_body)
    if path.isfile(plain_report_file_path):
        return _create_zip_file(plain_report_file_path, report_name, root_dir)
    return None

def _servers_compared_text(servers_compared):
    """Return a text ready to be printed on the email message.
    servers_compared is a dict of this form:
        { <main_server>: [list_of_servers_compared2the_main_server]}
    """
    sc_text = "\n\nCompared servers:\n\n"
    for main_server in servers_compared:
        sc_text += ("{0}: {1}\n".format(main_server, 
            ", ".join([server for server in servers_compared[main_server] \
                if server != main_server])))
    return sc_text

def send_report(emails, differences, conf_file, root_dir, servers_compared):
    """Send an email in which the content is the report coming in differences.
    differences is a dict of this form:
        { <other_server>: { <main_server != other_server>: [] } }
    Values in the list are the text messages of the files being different.
    """
    if emails:
        zip_report_file = _create_report_file(differences, conf_file, root_dir)
        subject = EMAIL_SUBJECT.format(conf_file=conf_file)
        email_addresses = " ".join(emails)
        sc_text = _servers_compared_text(servers_compared)
        send_email_command = SEND_EMAIL_COMMAND.format(subject=subject, 
                                        email_addresses=email_addresses)
        if not zip_report_file:
            email_body = "No differences were found." + sc_text
            command = SEND_EMAIL_NO_ATTACHMENT_COMMAND.format(
                                        email_body=email_body, 
                                        send_email_command=send_email_command)
        else:
            email_body = ("At least one difference was found.\n" + 
                          "Please check the attached report file.") + sc_text
            zip_name_attachment = path.basename(zip_report_file)
            command = SEND_EMAIL_ATTACHMENT_COMMAND.format(
                            email_body=email_body,
                            zip_path=zip_report_file, 
                            zip_name=zip_name_attachment, 
                            send_email_command=send_email_command)
        code = system(command)
        if code == 0:
            return {'msg': "Email sent to {0}".format(", ".join(emails))}
        return {'error': "Error happened when trying to send the email."}
    return {'msg': "No email addresses were defined."}
