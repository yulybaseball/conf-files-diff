"""
This module manages the creation of the list containing the complete path 
to the directories to be used for the comparison.
"""

from os import path, sep
import re

from pssapi.cmdexe import cmd

APPS_DOT_ALL_FILE = "/home/weblogic/applications.ALL"
COMMAND_CAT_APPS_DOT_ALL = "ssh -o ConnectTimeout=5 {server} \"cat {apps_dot_all_file}\""

def _get_apps_dot_all_directories(server, servers2compare):
    """Return a list with the directories specified in applications.ALL files 
    in main server (server parameter) and in every server on servers2compare.
    """
    all_servers = [server] + servers2compare
    directories = []
    def _dir(complete_file_path):
        return path.dirname(complete_file_path)
    for _server in all_servers:
        command = COMMAND_CAT_APPS_DOT_ALL.format(server=_server, 
                                        apps_dot_all_file=APPS_DOT_ALL_FILE)
        app_dot_all_content, error = cmd.cmdexec(command)
        if error.strip():
            raise Exception(error.strip())
        # assuming applications.ALL file has this content:
        # <app_name>:<directory>:<0|1>
        for line in app_dot_all_content.split('\n'):
            if line.strip():
                directories.append(_dir(line.split(':')[1]))
    return directories

def _exclude_directories(current_directories, exclude_directories):
    """Exclude directories from the list of current_directories. 
    Return the resulting list.
    """
    directories2remove = []
    for directory in current_directories:
        for exclude_directory in exclude_directories:
            if re.search(exclude_directory, directory):
                directories2remove.append(directory)
    set_directories2remove = set(directories2remove)
    set_current_directories = set(current_directories)
    return list(set_current_directories.difference(set_directories2remove))

def get_all_directories(server, server_conf):
    """Create a list with all directories to be used for the comparison.
    
    Keyword argument:
    server -- server defined in the conf file as the main one.
    server_conf -- dict with the configuration for the main server. It has 
        this form:
        {
            "compare2servers": [<other_server_names>],
            "directories": [<directories2compare>],
            "include_applications_dot_all": <true | false>,
            "include_files": [<rules_for_filtering_included_files>],
            "exclude_files": [<rules_for_filtering_excluded_files>],
            "by_structure": <true | false>
        }
    """
    all_directories = server_conf['directories']
    if server_conf['include_applications_dot_all']:
        all_directories = all_directories + _get_apps_dot_all_directories(
                                    server, server_conf['compare2servers'])
    if 'exclude_directories' in server_conf:
        all_directories = _exclude_directories(all_directories, 
                                            server_conf['exclude_directories'])
    return list(set(all_directories))
