"""
Compares files between servers, according to a configuration file.
It relies on 'comparer.py' script.
If defined, this will send an email with a report of the compared files.

Usage:

./cfdiff.py <configuration_file>

Configuration file should be in the 'conf' directory of this app:
    .
    ..
    conf/
        <configuration_file>

Just pass in the name of the conf file (no need for the complete path).
Conf file should have a JSON format with the following keys:

    {
    "servers": {
        <server_name>: {
            "compare2servers": [<other_servers2compare>],
            "directories": [<directories>],
            "include_applications_dot_all": <true | false>,
            "include_files": [<files_to_include>],
            "exclude_files": [<files_to_exclude>],
            "by_structure": <true | false>
        }
    },
    "settings": {
        "emails": [<email_addresses>]
    }
}

'servers': dictionary with the keys being the name of the servers.
'compare2servers': list of servers to include in the comparison.
'directories': list of complete paths to the directories where reside 
    the files to be compared.
'include_applications_dot_all': if true, directories on the file 
    applications.ALL will be included as part of the directories included 
    in the search. Please note this application will merge all directories 
    from all applications.ALL files on the servers listed down for comparison.
'include_files': names of the files to be included from the search.
'exclude_files': names of the files to be excluded from the search.
'by_structure': either true or false. Indicates if the files will be 
    compared line by line, or by structure. If compared by structure, 
    files should follow one of the file formats supported by 'comparer.py'.
'settings > emails': list containing the email addresses to send a report 
    to, after this app finishes comparing all servers.
"""

import json
from os import makedirs, path, system
import sys
import time

from pssapi.logger.logger import load_logger
from pssapi.cmdexe import cmd
from pssapi.emailer import emailer
from pssapi.utils import conf

logger = None
tmp_dir_by_run = None

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
CONF_DIR = path.join(EXEC_DIR, 'conf')
LOG_FILE_NAME = path.join(EXEC_DIR, 'logs', 'comparer.log')
TMP_DIR = path.join(EXEC_DIR, 'tmp')
TMP_DIR_BY_RUN = path.join(TMP_DIR, '{conf_file}-{current_time}')
TMP_DIR_SERVER = path.join('{tmp_dir_by_run}', '{server}')
REMOTE_EXE_FILE = path.join(EXEC_DIR, 'cfdiff', '_r_comparer.py')
DYNAMIC_REMOTE_EXE_FILE = path.join('{tmp_dir_server}', '_r_comparer.py')
SERVERS_USER = 'weblogic'
COPYING_COMMAND = 'rsync --no-p --no-g --chmod=ugo=rwX --relative {user}@{server}:"{files}" {location}'
COMPARER_SCRIPT = path.join(EXEC_DIR, 'comparer.py')
COMPARER_COMMAND = '{comparer_script} {file1} {file2} -vi{by_structure}'
COMPARER_SCRIPT_SAME_FILE_MSGE = "No differences were found."

def start(params):
    """Create the logger object."""
    global logger
    logger_fmtter = {
        'INFO': '%(asctime)s - %(message)s',
        'DEFAULT': '%(asctime)s - %(levelname)s - %(message)s'
    }
    logger = load_logger(log_file_name=LOG_FILE_NAME, **logger_fmtter)
    logger.info("Started comparison: {0}".format(params))

def finish(error=None):
    """Print error into log file. Remove temporal run folder. 
    Quit this script.
    """
    if error:
        try:
            logger.critical(error)
        except:
            pass
    if tmp_dir_by_run and path.exists(tmp_dir_by_run):
        logger.info("Removing temporal folder {0}".format(tmp_dir_by_run))
        system("rm -fr {0}".format(tmp_dir_by_run))
        if path.exists(tmp_dir_by_run):
            logger.warning(("Couldn't remove temporal folder {0}").\
                format(tmp_dir_by_run))
        else:
            logger.info(("Temporal folder {0} was removed.").\
                format(tmp_dir_by_run))
    logger.info("I finished... bye!")
    exit()

def _create_tmp_folder(config_file):
    """Create a folder with the name of the conf file passed in to this app 
    as parameter, and the current timestamp in seconds. Return the final name.
    This folder will be used to temporarily save all files to be compared for 
    the current run of this app. It has to be removed when finished.
    """
    current_time = time.time()
    dir_name = TMP_DIR_BY_RUN.format(conf_file=config_file, 
                                     current_time=current_time)
    try:
        makedirs(dir_name)
        if not path.exists(dir_name):
            raise IOError(("Error creating temp folder {0}.").\
                format(dir_name))
        logger.info("Temporal run folder was created: {0}".format(dir_name))
    except Exception as e:
        finish(error=str(e))
    return dir_name

def _create_tmp_script(server_tmp_folder):
    """Create the temp script inside the folder 'server_tmp_folder'.
    Return the file name (complete path).
    """
    _file = None
    tmp_exe_file = DYNAMIC_REMOTE_EXE_FILE.format(
                                            tmp_dir_server=server_tmp_folder)
    try:
        _file = open(tmp_exe_file, 'w+')
    except Exception as e:
        finish("Error creating tmp script in {0}: {1}".format(tmp_exe_file, 
                                                              str(e)))
    finally:
        if _file:
            _file.close()
    return tmp_exe_file

def _add_cont2remote_script(json_content, tmp_file):
    """Copy json_content to tmp_file."""
    json_content = "JSON_CONTENT = {0}".format(json_content)
    try:
        with open(REMOTE_EXE_FILE) as tmp_exe_f:
            with open(tmp_file, 'w') as tmp_dynamic_f:
                tmp_dynamic_f.write(json_content)
                for line in tmp_exe_f:
                    tmp_dynamic_f.write(line)
    except Exception as e:
        finish("Error copying content to temp script: {0}".format(str(e)))

def _rsync_from_remote_server(files, server, local_server_folder):
    """Copy files from server.

    Keyword arguments:
    files -- dict of this form:
        { <complete_file_name>: <hash_code | "txt"> }
    server -- server to pull the files from.
    local_server_folder -- local directory to copy the files to.

    It uses the rsync command to copy files from server.
    """
    def _escape_characters_file_name(_file_name):
        # characters needed to escape so rsync command doesn't fail copying
        esc_characters = {' ': '\\ ', ';': '\\;', ',': '\\,', ':': '\\:'}
        for esc_char in esc_characters:
            _file_name = _file_name.replace(esc_char, esc_characters[esc_char])
        return _file_name
    # copying only TXT files
    files = [file_name for file_name in files.keys() \
            if files[file_name] == 'txt' and \
            '"' not in file_name and "'" not in file_name]
    tmp_files = ""
    copies = 0
    logger.info("Copying files from server {0}".format(server))
    # as the files to be copied are concatenated (the whole path) to the rsync 
    # command, the final string is too long, so what follows is a hack to 
    # chunk the string and copy as many times as needed
    while files:
        _file = _escape_characters_file_name(files.pop(0))
        tmp_files += (_file + ' ')
        if len(tmp_files) >= 200000 or not files:
            copies += 1
            copying_command = COPYING_COMMAND.format(user=SERVERS_USER, 
                                                server=server,
                                                files=tmp_files, 
                                                location=local_server_folder)
            tmp_files = ""
            logger.info("rsync times: {0}".format(copies))
            dummy, error = cmd.cmdexec(copying_command)
            if error.strip():
                logger.error(error.strip())

def _retrieve_files(server, server_folders_config):
    """Copy locally the files found in every server by executing the remote 
    script.
    Files are copied keeping the whole path, starting at the server root 
    folder. Returns a dictionary with the files copied by server.
    """
    servers = server_folders_config['servers'].keys()
    server_script = server_folders_config['script']
    result_files = {}
    for _server in servers:
        logger.info("Executing remote script on {0}".format(_server))
        output, error = cmd.pythonexec(SERVERS_USER, _server, server_script, 
                                       True)
        if error.strip():
            logger.error(("While retrieving files from {0}, " + 
                          "this happened: {1}").format(_server, error.strip()))
        json_output = json.loads(output)
        if 'error' in json_output:
            logger.warning(("Skipped comparison between servers {0} and " + 
                            "{1} as files couldn't be retrieved due to " + 
                            "this error: {2}").\
                            format(server, _server, json_output['error']))
            if _server == server:
                # no need to copy more files as the 'main' comparison server
                # has no files to compare to
                return
            continue # retrieve files from other servers
        server_folder = server_folders_config['servers'][_server]
        _rsync_from_remote_server(json_output, _server, server_folder)
        logger.info(("Finished copying files from {0} to folder {1}").\
            format(_server, server_folder))
        result_files[_server] = json_output
    return result_files

def _create_server_folders_structure(server, server_conf):
    """Create root folder for holding stuff related to server. Create the 
    'personal' folder for every server specified under 'compare2servers' key 
    in server_conf, so the text files can be copied under every server's 
    folder.
    """
    # root folder for this specific server
    root_dir = TMP_DIR_SERVER.format(tmp_dir_by_run=tmp_dir_by_run, 
                                     server=server)
    try:
        makedirs(root_dir)
        if not path.exists(root_dir):
            raise IOError(("Error creating server root folder {0}.").\
                format(root_dir))
        logger.info("Server root folder was created: {0}".format(root_dir))
    except Exception as e:
        finish(error=str(e))
    logger.info("Creating directories list for main server {0}".format(server))
    from . import _search_directories
    all_directories = _search_directories.get_all_directories(server, 
                                                              server_conf)
    conf_values = {
        "directories": all_directories, 
        "exclude_files": server_conf['exclude_files'],
        "include_files": server_conf['include_files']
    }
    main_server_exe_script = _create_tmp_script(root_dir)
    _add_cont2remote_script(conf_values, main_server_exe_script)
    # inside the folder of this specific server, create another folder 
    # for this server, and the others coming in 'compare2servers' key
    all_servers = server_conf['compare2servers']
    all_servers.append(server)
    folders_conf = {'script': main_server_exe_script, 'servers': {}}
    for _server in all_servers:
        server_folder = path.join(root_dir, _server)
        try:
            makedirs(server_folder)
            if not path.exists(server_folder):
                raise IOError(("Error creating server folder {0}.").\
                    format(server_folder))
            folders_conf['servers'][_server] = server_folder 
            logger.info("Server folder was created: {0}".format(server_folder))
        except Exception as e:
            finish(error=str(e))
    return folders_conf

def _make_comparison(main_server_name, main_server_file, 
                      main_server_file_type, main_server_root_path,
                      other_server_name, other_server_files, 
                      other_server_root_path, by_structure):
    """Compare 2 specific files from 2 different servers.

    Keyword arguments:
    main_server_name -- name of the main server
    main_server_file -- name of the file in the main server. This is the 
        complete path of the file in the main server. It doesn't contain the 
        root path for the main server when running this app.
    main_server_file_type -- either the hash code of the main server file, or 
        the word 'txt'.
    main_server_root_path -- complete path of the root folder for the main 
        server when running this app.
    other_server_name -- name of the second server.
    other_server_files -- dictionary containing all files in the second server.
        It has this form:
        { <complete_file_path>: <hash_code | 'txt'> }
        complete_file_path is the path of the file in the second server, it 
        doesn't include the root path created for this server when running 
        this app.
    other_server_root_path -- complete path of the root folder for the second 
        server when running this app.
    by_structure -- defines if both files should be compared by structure.

    Return a text containing the result of comparing 2 files. If comparison 
    didn't find differences, return an empty string.
    """
    result = ""
    if main_server_file in other_server_files:
        other_server_file_type = other_server_files[main_server_file]
        if main_server_file_type != other_server_file_type and \
                (main_server_file == 'txt' or other_server_file_type == 'txt'):
            # different types
            return "File types are different: {0}".format(main_server_file)
        if main_server_file_type == 'txt':
            # same type, both are txt
            # remove first slash from the main server file path as  
            # function os.path.join discards previous path components if 
            # any of the next ones is an absolute path
            cmpte_path_main_server_file = path.join(main_server_root_path, 
                                                    main_server_file[1:])
            cmpte_path_other_server_file = path.join(other_server_root_path, 
                                                    main_server_file[1:])
            bstrctr = "s" if by_structure else ""
            comparer_command = COMPARER_COMMAND.format(
                comparer_script=COMPARER_SCRIPT,
                file1=cmpte_path_main_server_file,
                file2=cmpte_path_other_server_file,
                by_structure=bstrctr
            )
            output, dummy = cmd.cmdexec(comparer_command)
            clean_output = output.strip()
            if clean_output != COMPARER_SCRIPT_SAME_FILE_MSGE:
                df = "Files are different."
                if clean_output.find(df) != -1:
                    clean_output = clean_output.replace(df, 
                    "{0}: {1}".format(df.replace('.', ''), main_server_file))
                result = clean_output
        elif main_server_file_type != other_server_file_type:
            # same type, both are binary
            result = ("Files are different (compared by hash codes): {0}").\
                     format(main_server_file)
    else:
        result = "{0} is not in {1}".format(main_server_file, other_server_name)
    return result + '\n' if result else result

def _compare(main_server, servers_paths, files, by_structure):
    """Compare every file between servers.

    Keyword arguments:
    main_server -- name of the main server for the comparison.
    servers_paths -- dictionary with the keys being the name of the servers 
        and the values being the root folder of the server.
    files -- dictionary of this form:
        { <server>: { <complete_file_path>: <hash | 'txt'> } }
    by_structure -- whether this comparison should be made using the 
        structure of the files, and not line by line

    Return a dictionary of this form:
        { <main_server != other_server>: [] }
    Values in the list are the text messages of the files being different.
    """
    servers_comparison = {}
    server_names = sorted(files.keys())
    server_names.remove(main_server)
    main_server_files = files.pop(main_server, [])
    main_server_root_path = servers_paths[main_server]
    for server_name in server_names:
        logger.info("Comparing {0} and {1}".format(main_server, server_name))
        diff_txt = "{0} != {1}".format(main_server, server_name)
        servers_comparison[diff_txt] = []
        for _file, file_type in main_server_files.iteritems():
            text_result = _make_comparison(main_server, _file, file_type, 
                            main_server_root_path, server_name, 
                            files[server_name], servers_paths[server_name], 
                            by_structure)
            if text_result:
                servers_comparison[diff_txt].append(text_result)
            # remove file from secondary server
            # by keeping only not-compared files, it can be checked if 
            # there are files in secondary servers that are not present 
            # in the main server
            try:
                del files[server_name][_file]
            except:
                pass
        # check whether there are remaining files on the second server dict,
        # in which case means second server has files not present in main 
        # server
        if files[server_name]:
            for _f in files[server_name]:
                mss = "File {0} is not in server {1}\n".format(_f, main_server)
                servers_comparison[diff_txt].append(mss)
        logger.info("Done comparing {0} and {1}".format(main_server, 
                                                        server_name))
    return servers_comparison

def _send_report(emails, differences, config_file, servers_compared):
    """Send report by email."""
    logger.info("Preparing report and email...")
    from . import _report
    result = _report.send_report(emails, differences, config_file, 
                                 tmp_dir_by_run, servers_compared)
    if 'error' in result:
        logger.error(result['error'])
    else:
        logger.info(result['msg'])

def compare(config_file):
    """Compare files on servers as configured in config_file.
    config_file is the name of the configuration file passed in as parameter 
    when executing this app.
    """
    conf_file_complete_path = path.join(CONF_DIR, config_file)
    if not path.isfile(conf_file_complete_path):
        raise IOError()
    conf_file_json = conf.get_conf(conf_file_complete_path) 
    try:
        emails = conf_file_json['settings']['emails']
        global tmp_dir_by_run
        tmp_dir_by_run = _create_tmp_folder(config_file)
        servers = sorted(conf_file_json['servers'].keys())
        differences = {}
        servers_compared = {}
        for server in servers:
            server_conf = conf_file_json['servers'][server]
            servers_compared[server] = server_conf['compare2servers']
            by_structure = server_conf['by_structure']
            server_folders_config = _create_server_folders_structure(server, 
                                                                server_conf)
            files = _retrieve_files(server, server_folders_config)
            differences[server] = _compare(server, 
                                           server_folders_config['servers'], 
                                           files, by_structure)
        logger.info("Comparison finished.")
        _send_report(emails, differences, config_file, servers_compared)
    except KeyError as ke:
        finish("Conf file must have key {0}.".format(str(ke)))
    except Exception as e:
        finish(str(e))
