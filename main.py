#!/bin/env python

import sys
from os import path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from cfdiff import cfdiff

def _main():
    error = None
    params = sys.argv
    try:
        cfdiff.start(params)
        conf_file = params[1]
        cfdiff.compare(conf_file)
    except IndexError:
        error = "Missing parameter conf file. Please see Usage section."
    except IOError:
        error = "Input/Output error. Please check conf file exists."
    except Exception as e:
        error = e.__str__()
    cfdiff.finish(error)

if __name__ == "__main__":
    _main()
