#!/bin/env python
# -*- coding: utf-8 -*-

"""
Compares binary files by their checksum, or text files line by line or 
by structure.
So far, these are the supported formats when comparing by structure: 
    XML, YAML and JSON.

Usage:

./comparer.py <file1> <file2> [-s] [-v] [-i]

file1 and file2 are the files to be compared.
-s: indicates if files should be compared by structure. If not specified, 
    files will  be compared line by line.
-v: verbose. Outputs the differences. Please note that if this option is 
    specified, the output might be huge if files are big and with many 
    differences.
-i: show invisible (non-printable) characters. 
    Only usefull when -v option is specified and files are not binary.
"""

import getopt
from os import path
import sys

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from pssapi.utils import util

OPTIONS = "svi"
#  These are the characters to be replaced
#  SPACE -> ·
#  TAB -> »
#  RETURN/NEW LINE -> ¶
INVISIBLE_CHARACTERS = {' ': '\xb7', '\t': '\xbb', '\n': '\xb6'}

def _quit(message, ecode=0):
    """Print message and exit this script with ecode."""
    print(message)
    exit(ecode)

def _compare_by_structure(file1, file2, verbose, show_inv):
    print("inside _compare_by_structure")

def _files_text(file1, file2, listed=False):
    """Return a tuple with the content of file1 and file2. If listed is True, 
    content will be returned as a list containing every line of the files.
    """
    with open(file1) as f1:
        f1cnt = f1.read() if not listed else f1.readlines()
    with open(file2) as f2:
        f2cnt = f2.read() if not listed else f2.readlines()
    return f1cnt, f2cnt

def _same_binary_content(file1, file2):
    """Compare wheter the hash for both files are the same."""
    return util.file_hash(file1) == util.file_hash(file2)

def _print_differences(diffs, show_inv):
    """Print differences coming in parameter diffs.

    Keyword arguments:
    diffs -- Dictionary of this form:
        <line_number>: [<text_in_file1>, <text_in_file2>]
        - line_number represents the line being different between the files. 
        In case one file had more lines than the other, line_number represents 
        the line in the bigger file.
    show_inv -- Defines if the invisible (non-printible) characters are shown.
    """
    def _show_inv_chars(txt):
        final_text = txt
        for _key in INVISIBLE_CHARACTERS.keys():
            final_text = final_text.replace(_key, 
                INVISIBLE_CHARACTERS[_key].encode('hex').decode('hex'))
        return final_text

    diff_lines_number = sorted([int(line) for line in diffs.keys()])
    for line_num in diff_lines_number:
        key_line_num = str(line_num)
        if show_inv:
            f1l = _show_inv_chars(diffs[key_line_num][0])
            f2l = _show_inv_chars(diffs[key_line_num][1])
        else:
            f1l = diffs[key_line_num][0].replace('\n', '')
            f2l = diffs[key_line_num][1].replace('\n', '')
        diff = "Line {0}:\nFile1: {1}\nFile2: {2}".format(key_line_num, 
                                                            f1l, f2l)
        print(diff)

def _verbose_line_by_line_comparing(file1, file2, show_inv):
    """Compare line by line of files file1 and file2. Print invisible 
    (non-printable) characters if show_inv is True.
    """
    # at this point, if at least one of the files is a text file, it's 
    # because both are text files
    if not util.is_binary(file1):
        result = {}
        f1lines, f2lines = _files_text(file1, file2, listed=True)
        for index, f1line in enumerate(f1lines):
            try:
                f2line = f2lines[index]
            except:
                f2line = ""
            if f1line != f2line:
                result[str(index + 1)] = [f1line, f2line]
        # iterate through second file (in case it had more lines than the 
        # first one)
        f1len = len(f1lines)
        if f1len < len(f2lines):
            for index, f2line in enumerate(f2lines[f1len:]):
                result[str(index + f1len + 1)] = ["", f2line]
        _print_differences(result, show_inv)

def _same_size(file1, file2):
    """Return whether file1 and file2 have the same size."""
    return path.getsize(file1) == path.getsize(file2)

def _same_content(file1, file2):
    """Return whether file1 and file2 have the same content.
    If both files are binary, comparison will be made using the hash."""
    # at this point, if at least one of the files is binary, it's because both 
    # are binary, so comparison is made by the checksum
    if util.is_binary(file1):
        return _same_binary_content(file1, file2)
    # comparison by content
    f1txt, f2txt = _files_text(file1, file2)
    return f1txt == f2txt

def _same_type(file1, file2):
    """Return whether file1 and file2 are of the same type (both binary or 
    both text files).
    """
    return util.is_binary(file1) == util.is_binary(file2)

def _compare(file1, file2, structure, verbose, show_inv):
    """Compare the content of 2 files.

    Keyword arguments:
    file1 -- Complete path to one of the files to be compared.
    file2 -- Complete path to the other file to be compared.
    structure -- True or False. Defines if comparison will be made by 
        a configuration file structure or line by line.
    verbose -- True or False. Print the differences between the files.
    show_inv -- True or False. Print invisible characters, i.e. spaces, tabs, 
        new lines... Not used if verbose is False.
    """
    if structure:
        _compare_by_structure(file1, file2, verbose, show_inv)
    else:
        if not _same_size(file1, file2) or not _same_type(file1, file2) or \
                not _same_content(file1, file2):
            print("Files are different.")
            if verbose:
                _verbose_line_by_line_comparing(file1, file2, show_inv)
        else:
            print("No differences were found.")

def _check_params(params):
    """Check params passed in to this script are valid.
    Return a dict of this form:
        {
            'file1': <complete_path2file1>,
            'file2': <complete_path2file2>,
            'structure': <True | False>,
            'verbose': <True | False>,
            'inv_chars': <True | False>
        }
    """
    conf_options = {}
    try:
        options, values = getopt.gnu_getopt(params, OPTIONS)
        conf_options['file1'] = values[0]
        conf_options['file2'] = values[1]
        if not path.isfile(conf_options['file1']) or \
           not path.isfile(conf_options['file2']):
            raise IOError("Error: One of the files doesn't exist.")
        opts_list = [option[0] for option in options]
        conf_options['structure'] = '-s' in opts_list
        conf_options['verbose'] = '-v' in opts_list
        conf_options['inv_chars'] = '-i' in opts_list
    except IndexError:
        _quit("Missing parameters. " + 
              "Please check Usage section of this script.", 1)
    except Exception as e:
        _quit(str(e), 1)
    return conf_options

def _main():
    params = sys.argv[1:]
    conf_options = _check_params(params)
    _compare(conf_options['file1'], conf_options['file2'], 
             conf_options['structure'], conf_options['verbose'], 
             conf_options['inv_chars'])

if __name__ == "__main__":
    _main()
